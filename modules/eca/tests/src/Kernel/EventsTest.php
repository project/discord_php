<?php

namespace Drupal\Tests\discord_php_eca\Kernel;

use Discord\Discord;
use Discord\Factory\Factory;
use Discord\Http\Http;
use Discord\Parts\Channel\Channel;
use Discord\Parts\Channel\Message;
use Discord\Parts\User\User;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\discord_php\Event\DiscordEvents;
use Drupal\discord_php\Event\MessageCreateEvent;
use Drupal\discord_php\Event\ReadyEvent;
use Drupal\discord_php\Services\DiscordPhpManager\DiscordPhpManagerInterface;
use Drupal\eca\Entity\Eca;
use Drupal\eca_test_array\Plugin\Action\ArrayWrite;
use Drupal\KernelTests\KernelTestBase;
use React\Promise\ExtendedPromiseInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Kernels tests for reacting upon events provided by "discord_php_eca".
 *
 * @group discord_php
 * @group discord_php_eca
 */
class EventsTest extends KernelTestBase {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The UUID generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected UuidInterface $uuid;

  /**
   * The channel.
   *
   * @var \Discord\Parts\Channel\Channel
   */
  protected Channel $channel;

  /**
   * The Discord service.
   *
   * @var \Discord\Discord
   */
  protected Discord $discord;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'discord_php',
    'discord_php_eca',
    'discord_php_eca_test_models',
    'eca',
    'eca_test_array',
    'key',
    'serialization',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');

    $this->installConfig(static::$modules);

    $this->eventDispatcher = $this->container->get('event_dispatcher');
    $this->uuid = $this->container->get('uuid');

    $this->initDiscord();
  }

  /**
   * Test ready-event.
   */
  public function testReadyEvent(): void {
    $this->eventDispatcher->dispatch(new ReadyEvent($this->discord), DiscordEvents::READY);

    $config = Eca::load('process_fcj40qs');
    $this->assertNotNull($config->uuid());

    $this->assertEquals(DiscordEvents::READY, ArrayWrite::$array['discord_php_ready_event']);
    $this->assertEquals('ready', ArrayWrite::$array['discord_php_status']);
  }

  /**
   * Test message_create-event.
   */
  public function testMessageCreatedEvent(): void {
    $channelId = $this->uuid->generate();
    $this->initDiscord($channelId);
    $message = $this->generateMessage($channelId);

    $manager = $this->prophesize(DiscordPhpManagerInterface::class);
    $manager->getClient()->willReturn($this->discord);
    \Drupal::getContainer()->set('discord_php.services.discord_php_manager', $manager->reveal());

    $this->eventDispatcher->dispatch(new MessageCreateEvent($this->discord, $message), DiscordEvents::MESSAGE_CREATE);

    $config = Eca::load('process_fcj40qs');
    $this->assertNotNull($config->uuid());

    $this->assertEquals(DiscordEvents::MESSAGE_CREATE, ArrayWrite::$array['discord_php_message_create_event']);
    $this->assertEquals(sprintf('%s: %s (%s)', $message->id, $message->content, $message->user_id), ArrayWrite::$array['discord_php_message_id']);
  }

  /**
   * Initialize Discord.
   *
   * @param string|null $channelId
   *   The optional channel ID.
   */
  protected function initDiscord(?string $channelId = NULL): void {
    /** @var \Discord\Discord|\Prophecy\Prophecy\ObjectProphecy $discord */
    $discord = $this->prophesize(Discord::class);
    $discord->getFactory()->willReturn($this->prophesize(Factory::class)->reveal());
    $discord->getHttpClient()->willReturn($this->prophesize(Http::class)->reveal());

    if (!empty($channelId)) {
      $promise = $this->prophesize(ExtendedPromiseInterface::class)
        ->reveal();
      $channel = $this->createPartialMock(Channel::class, ['sendMessage']);
      $channel->fill(['id' => $channelId]);
      $channel->expects($this->once())
        ->method('sendMessage')
        ->willReturn($promise);

      $this->channel = $channel;
      $discord->getChannel($channelId)->willReturn($this->channel);
    }

    $this->discord = $discord->reveal();
  }

  /**
   * Generate a message.
   *
   * @param string|null $channelId
   *   The optional channel ID.
   *
   * @return \Discord\Parts\Channel\Message
   *   Returns the generated message.
   */
  protected function generateMessage(?string $channelId = NULL): Message {
    $user = new User($this->discord);
    $user->id = $this->uuid->generate();

    if (empty($this->channel) && !empty($channelId)) {
      $this->channel = new Channel($this->discord);
      $this->channel->id = $channelId;
    }

    $message = new Message($this->discord);
    $message->id = $this->uuid->generate();
    $message->author = $user;
    $message->channel = $this->channel;
    $message->channel_id = $this->channel?->id ?? '';
    $message->content = $this->randomString();
    $message->timestamp = time();

    return $message;
  }

}
