<?php

namespace Drupal\discord_php_eca\Plugin\Action;

use Discord\Builders\MessageBuilder;
use Discord\Parts\Channel\Message;
use Drupal\Core\Form\FormStateInterface;
use Drupal\discord_php\Services\DiscordPhpManager\DiscordPhpManagerInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Action to perform message sending.
 *
 * @Action(
 *   id = "discord_php_eca_send_message",
 *   label = @Translation("Send message"),
 *   category = @Translation("DiscordPHP")
 * )
 */
class SendMessageAction extends ConfigurableActionBase {

  /**
   * The DiscordPHP manager.
   *
   * @var \Drupal\discord_php\Services\DiscordPhpManager\DiscordPhpManagerInterface
   */
  protected DiscordPhpManagerInterface $discordPhpManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->discordPhpManager = $container->get('discord_php.services.discord_php_manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $discord = $this->discordPhpManager->getClient();

    if (
      !empty($this->configuration['reply_to_id'])
      && !empty($this->configuration['reply_to_channel_id'])
    ) {
      $replyTo = new Message($discord, [
        'id' => $this->tokenService->replace($this->configuration['reply_to_id']),
        'channel_id' => $this->tokenService->replace($this->configuration['reply_to_channel_id']),
      ]);
    }

    $message = (MessageBuilder::new())
      ->setContent($this->tokenService->replace($this->configuration['content']));
    if (isset($replyTo)) {
      $message->setReplyTo($replyTo);
    }

    if (!empty($this->configuration['filepath'])) {
      $filepath = $this->tokenService->replace($this->configuration['filepath']);
      $message->addFile($filepath);
    }

    $discord->getChannel($this->tokenService->replace($this->configuration['channel_id']))
      ->sendMessage($message);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'content' => '',
      'channel_id' => '',
      'reply_to_id' => '',
      'reply_to_channel_id' => '',
      'filepath' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['content'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content'),
      '#default_value' => $this->configuration['content'],
      '#required' => TRUE,
    ];

    $form['channel_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Channel ID'),
      '#default_value' => $this->configuration['channel_id'],
      '#required' => TRUE,
    ];

    $form['reply_to_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reply-To ID'),
      '#default_value' => $this->configuration['reply_to_id'],
    ];

    $form['reply_to_channel_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reply-To Channel ID'),
      '#default_value' => $this->configuration['reply_to_channel_id'],
    ];

    $form['filepath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filepath'),
      '#default_value' => $this->configuration['filepath'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    if (empty($form_state->getValue('content'))) {
      $form_state->setErrorByName('content', $this->t('Content of the message can not be empty.'));
    }

    if (empty($form_state->getValue('channel_id'))) {
      $form_state->setErrorByName('content', $this->t('Channel ID of the message is not correct.'));
    }

    if (
      empty($form_state->getValue('reply_to_id'))
      && !empty($form_state->getValue('reply_to_channel_id'))
    ) {
      $form_state->setErrorByName('reply_to_id', $this->t('A reply-to channel ID without a reply-to ID is not allowed.'));
    }

    if (
      !empty($form_state->getValue('reply_to_id'))
      && empty($form_state->getValue('reply_to_channel_id'))
    ) {
      $form_state->setErrorByName('reply_to_channel_id', $this->t('A reply-to ID without a reply-to channel ID is not allowed.'));
    }

    if (!empty($form_state->getValue('filepath'))) {
      $filepath = $this->tokenService->replace($form_state->getValue('filepath'));
      if (!file_exists($filepath)) {
        $form_state->setErrorByName('filepath', $this->t('Could not verify the specified filepath.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['content'] = $form_state->getValue('content');
    $this->configuration['channel_id'] = $form_state->getValue('channel_id');
    $this->configuration['reply_to_id'] = $form_state->getValue('reply_to_id');
    $this->configuration['reply_to_channel_id'] = $form_state->getValue('reply_to_channel_id');
    $this->configuration['filepath'] = $form_state->getValue('filepath');

    parent::submitConfigurationForm($form, $form_state);
  }

}
