<?php

namespace Drupal\discord_php_eca\Plugin\ECA\Event;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\Core\Utility\Error;
use Drupal\discord_php\Event\DiscordEvents;
use Drupal\discord_php\Event\MessageCreateEvent;
use Drupal\discord_php\Event\ReadyEvent;
use Drupal\discord_php\TypedData\Definition\MessageDefinition;
use Drupal\eca\Attributes\Token;
use Drupal\eca\Plugin\DataType\DataTransferObject;
use Drupal\eca\Plugin\ECA\Event\EventBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Plugin implementation of the ECA events for discord_php_eca.
 *
 * @EcaEvent(
 *   id = "discord_php",
 *   deriver = "Drupal\discord_php_eca\Plugin\ECA\Event\DiscordPhpEventDeriver"
 * )
 */
class DiscordPhpEvent extends EventBase {

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected TypedDataManagerInterface $typedDataManager;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected NormalizerInterface $serializer;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->typedDataManager = $container->get('typed_data_manager');
    $instance->serializer = $container->get('serializer');
    $instance->logger = $container->get('logger.channel.discord_php');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    $definitions = [];

    $definitions['ready'] = [
      'label' => 'Ready',
      'event_name' => DiscordEvents::READY,
      'event_class' => ReadyEvent::class,
    ];

    $definitions['message_create'] = [
      'label' => 'Message: create',
      'event_name' => DiscordEvents::MESSAGE_CREATE,
      'event_class' => MessageCreateEvent::class,
    ];

    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  #[Token(
    name: 'message',
    description: 'The message that was created.',
    classes: [
      MessageCreateEvent::class,
    ],
    properties: [
      new Token(name: 'id', description: 'The unique identifier of the message.'),
      new Token(name: 'channel_id', description: 'The unique identifier of the channel that the message was went in.'),
      new Token(name: 'guild_id', description: 'The unique identifier of the guild that the channel the message was sent in belongs to.'),
      new Token(name: 'user_id', description: 'The user id of the author.'),
      new Token(name: 'content', description: 'The content of the message if it is a normal message.'),
      new Token(name: 'timestamp', description: 'A timestamp of when the message was sent, in RFC3339-format.'),
    ],
  )]
  public function getData(string $key): mixed {
    $event = $this->event;

    switch ($key) {
      case 'message':
        if ($event instanceof MessageCreateEvent) {
          try {
            $definition = MessageDefinition::create('discord_message');

            return DataTransferObject::create($this->typedDataManager->create(
              $definition,
              $this->serializer->normalize($event->getMessage())
            ));
          }
          catch (ExceptionInterface $e) {
            $this->logger->error(Error::DEFAULT_ERROR_MESSAGE, Error::decodeException($e));
          }
        }
        break;
    }

    return parent::getData($key);
  }

}
